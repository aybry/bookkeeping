import EntityCard from "../../components/addressBook/EntityCard.vue";


// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: "addressBook/EntityCard",
  component: EntityCard,
  // More on argTypes: https://storybook.js.org/docs/vue/api/argtypes
  argTypes: {
    entity: {}
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args) => ({
  components: { EntityCard },
  setup() {
    return { args };
  },
  template: "<entity-card :entity='args.entity' />",
});

export const Filled = Template.bind({});
// More on args: https://storybook.js.org/docs/vue/writing-stories/args
Filled.args = {
  label: "EntityCard",
  entity: {
    display_name: "Sam",
    id: "123",
    address: {
      line_one: "123 fake street",
      postcode: "10247",
      city: "berlin",
    }
  }};
