import axiosInstance from "@/services/api";


const namespaced = true;
const state = {
  open: [],
}
const getters = {
  active: (state) => (state.open.length > 0 ? state.open[0] : null),
  allOpen: (state) => state.open,
}
const mutations = {
  OPEN: (state, payload) => state.open.unshift(payload),
  CLOSE: (state, payload) => (state.open = state.open.filter((e) => e !== payload)),
}
const actions = {
  open: ({ commit }, payload) => commit('OPEN', payload),
  close: ({ commit }, payload) => commit('CLOSE', payload),
}

const modalStoreModule = {
  namespaced,
  state,
  mutations,
  actions,
  getters,
};

export default modalStoreModule;
