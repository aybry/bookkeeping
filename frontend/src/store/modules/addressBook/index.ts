import axiosInstance from "@/services/api";

const namespaced = true;

const state = {
  entities: [],
  current: null,
  status: "",
  last_update: "",
};

const getters = {
  data: (state) => state.entities,
  current: (state) => state.current,
  options: (state) =>
    state.entities.map((entity) => {
      return { label: entity.display_name, value: entity.id };
    }),
};

const mutations = {
  request(state) {
    state.status = "loading";
  },

  fetch_success(state, payload) {
    state.status = "success";
    state.entities = payload;
    state.last_update = new Date();
    console.log(state.entities[0]);
  },

  create_success(state, payload) {
    console.log(payload);
    state.status = "success";
    state.entities.push(payload);
  },

  update_success(state, payload) {
    state.status = "success";
    const index = state.entities.findIndex(
      (entity) => entity.index == payload.id
    );
    state.entities[index] = payload;
  },

  delete_success(state, payload) {
    const index = state.entities.findIndex((entity) => entity.id == payload.id);
    state.entities.splice(index, 1);
    state.status = "success";
  },

  error(state, payload) {
    state.status = "error";
  },

  new_current(state, payload) {
    state.current = payload;
  },

  sort(state) {
    state.entities = state.entities.sort((firstEl, secondEl) => {
      const firstName = firstEl.display_name.toLowerCase();
      const secondName = secondEl.display_name.toLowerCase();
      if (firstName > secondName) {
        return 1;
      } else if (firstName < secondName) {
        return -1;
      } else if (firstName == secondName) {
        return 0;
      }
    });
  },
};

const actions = {
  get({ commit }, entityId) {
    commit("request");
    axiosInstance.get(`/entities/${entityId}`).then((res) => {
      commit("new_current", res.data);
      console.log(res.data);
    });
  },

  fetch({ commit }) {
    commit("request");
    console.log("fetching");
    axiosInstance.get("/entities/").then((res) => {
      commit("fetch_success", res.data);
      console.log("fetched");
      commit("sort");
    });
  },

  create: ({ commit }, entity) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .post("/entities/", entity)
        .then((resp) => {
          const entity = resp.data;
          commit("create_success", entity);
          commit("new_current", entity);
          commit("sort");
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  update: ({ commit }, entity) => {
    commit("request");
    console.log("entity");
    console.log(entity);
    return new Promise((resolve, reject) => {
      axiosInstance
        .put(`/entities/${entity.id}/`, entity)
        .then((resp) => {
          const entity = resp.data;
          commit("update_success", entity);
          commit("sort");
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  delete: ({ commit }, entity) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .delete(`/entities/${entity.id}/`, entity)
        .then((resp) => {
          commit("delete_success", entity);
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  set_current_by_id({ commit }, entity_id) {
    const entity = state.entities.filter((entity) => entity.id == entity_id)[0];
    commit("new_current", entity);
  },
};

const entitiestoreModule = {
  namespaced,
  state,
  mutations,
  actions,
  getters,
};

export default entitiestoreModule;
