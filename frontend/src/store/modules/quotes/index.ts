import axiosInstance from "@/services/api";

const namespaced = true;

const state = {
  quotes: [],
  current: null,
  status: "",
  last_update: "",
};

const getters = {
  data: (state) => state.quotes,
  current: (state) => state.current,
};

const mutations = {
  request(state) {
    state.status = "loading";
  },

  fetch_success(state, payload) {
    state.status = "success";
    state.quotes = payload;
    state.last_update = new Date();
  },

  create_success(state, payload) {
    state.status = "success";
    state.quotes.unshift(payload);
  },

  update_success(state, payload) {
    state.status = "success";
    const quoteIndex = state.quotes.findIndex(
      (quote) => quote.id == payload.id
    );
    state.quotes[quoteIndex] = payload;
  },

  delete_success(state, payload) {
    state.status = "success";
    const quoteIndex = state.quotes.findIndex((quote) => quote.id == payload);
    state.quotes.splice(quoteIndex, 1);
  },

  error(state, payload) {
    state.status = "error";
  },

  new_current(state, payload) {
    state.current = payload;
  },
};

const actions = {
  get({ commit }, quoteId) {
    commit("request");
    axiosInstance.get(`/quotes/${quoteId}`).then((res) => {
      commit("new_current", res.data);
      console.log(res.data);
    });
  },

  fetch({ commit }) {
    commit("request");
    axiosInstance.get("/quotes/").then((res) => {
      commit("fetch_success", res.data);
      console.log(res.data);
    });
  },

  create: ({ commit }, quote) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .post("/quotes/", quote)
        .then((resp) => {
          const quote = resp.data;
          commit("create_success", quote);
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  update: ({ commit }, quote) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .put(`/quotes/${quote.id}/`, quote)
        .then((resp) => {
          const quote = resp.data;
          commit("update_success", quote);
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  delete: ({ commit }, quote) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .delete(`/quotes/${quote.id}/`, quote)
        .then((resp) => {
          commit("delete_success", resp.config.id);
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  set_current({ commit }, quoteId) {
    commit("new_current", quoteId);
  },
};

const quoteStoreModule = {
  namespaced,
  state,
  mutations,
  actions,
  getters,
};

export default quoteStoreModule;
