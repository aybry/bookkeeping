import { createApp } from "vue";

import App from "./App.vue";
import VueAxios from "vue-axios";
import naive from "naive-ui";

import store from "./store";
import router from "./router";
import axiosInstance from "./services/api";

createApp(App)
  .use(store)
  .use(router)
  .use(VueAxios, axiosInstance)
  .mount("#app");
