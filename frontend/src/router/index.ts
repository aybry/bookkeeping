import { createRouter, createWebHistory } from "vue-router";
import store from "@/store";

import Home from "@/views/Home.vue";
import Items from "@/views/Items.vue";
import AddressBook from "@/views/AddressBook.vue";
import InvoiceList from "@/views/invoices/InvoiceList.vue";
import QuoteList from "@/views/quotes/QuoteList.vue";
import DocumentDetail from "@/views/documents/DocumentDetail.vue";
import Login from "@/views/Login.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/items",
    name: "items",
    component: Items,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/address-book",
    name: "address-book",
    component: AddressBook,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/invoices",
    name: "invoices",
    component: InvoiceList,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/invoices/:invoiceId",
    name: "invoice-detail",
    component: DocumentDetail,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/quotes",
    name: "quotes",
    component: QuoteList,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/quotes/:quoteId",
    name: "quote-detail",
    component: DocumentDetail,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const authRequired = to.matched.some((record) => record.meta.requiresAuth);
  const isAuthenticated = store.getters["auth/isAuthenticated"];

  if (authRequired && !isAuthenticated) {
    next({ name: "login", query: { redirect: from.path } });
  } else if (isAuthenticated && to.path === "/login") {
    next({ name: "invoices" });
  } else {
    next();
  }
});

export default router;
