import axios from "axios";
import store from "@/store";
import router from "@/router";

const axiosInstance = axios.create();

axiosInstance.defaults.baseURL = "http://bookkeeping.localhost:8000/";
axiosInstance.defaults.headers.post["Content-Type"] = "application/json";

axiosInstance.interceptors.request.use(
  (config) => {
    const isAuthenticated = store.getters["auth/isAuthenticated"];
    if (!!localStorage.getItem("token")) {
      const token = localStorage.getItem("token");
      config.headers = Object.assign({
        Authorization: `Bearer ${token}`,
      });
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;
    if (
      error.response.status === 401 &&
      error.response.config.url === "api/token/refresh/"
    ) {
      await store.dispatch("auth/logout");
      router.push({ name: "login" });
    } else if (error.response.status === 401) {
      const token = await store.dispatch("auth/refresh");
      axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
      return axiosInstance(originalRequest);
    }
    return Promise.reject(error);
  }
);
export default axiosInstance;
