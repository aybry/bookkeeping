import uuid
from typing import List

from django.db import models
from django.contrib.auth.models import User
from django_iban.fields import IBANField, SWIFTBICField
import phonenumbers

from .validators import phone_number_validator


class Entity(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    display_name = models.CharField(max_length=128)
    slug = models.CharField(max_length=32, default="", unique=True)
    address = models.ForeignKey("Address", on_delete=models.SET_NULL, null=True)
    email_address = models.EmailField(blank=True, null=True)
    phone_number = models.CharField(
        max_length=32, validators=[phone_number_validator], null=True, blank=True
    )
    tax_number = models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        suffix = "" if len(self.display_name) < 32 else "..."
        return f"{self.display_name:.32}{suffix}"

    @property
    def phone_number_formatted(self):
        try:
            phone_no = phonenumbers.parse(self.phone_number)
            return f"+{phone_no.country_code} {phone_no.national_number}"
        except Exception as exc:
            print(exc)
            return self.phone_number

    @property
    def address_lines(self):
        address_lines = [
            self.display_name,
            self.address.line_one,
        ]
        if self.address.line_two:
            address_lines.append(self.address.line_two)
            if self.address.line_three:
                address_lines.append(self.address.line_three)

        address_lines.append(f"{self.address.postcode} {self.address.city}")

        if self.address.state:
            address_lines.append(self.address.state)

        address_lines.append(self.address.country)

        return address_lines


class Address(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    line_one = models.CharField(max_length=128, blank=True)
    line_two = models.CharField(max_length=128, blank=True)
    line_three = models.CharField(max_length=128, blank=True)
    postcode = models.CharField(max_length=32, blank=True)
    city = models.CharField(max_length=128, blank=True)
    state = models.CharField(max_length=128, blank=True)
    country = models.CharField(max_length=128, blank=True)

    def __str__(self):
        suffix = "" if len(self.line_one) < 32 else "..."
        return f"{self.line_one:.32}{suffix}"


class BankAccount(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    owner = models.CharField(max_length=128)
    name = models.CharField(max_length=128, blank=True)
    iban = IBANField()
    bic = SWIFTBICField()

    @property
    def iban_formatted(self):
        return " ".join(self.iban[i : i + 4] for i in range(0, len(self.iban), 4))
