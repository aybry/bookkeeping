# Generated by Django 3.2.14 on 2022-07-24 00:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address_book', '0003_bankaccount_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='line_three',
            field=models.CharField(blank=True, max_length=128),
        ),
    ]
