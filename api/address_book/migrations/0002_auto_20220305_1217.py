# Generated by Django 3.2.10 on 2022-03-05 12:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address_book', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='city',
            field=models.CharField(blank=True, max_length=128),
        ),
        migrations.AlterField(
            model_name='address',
            name='country',
            field=models.CharField(blank=True, max_length=128),
        ),
        migrations.AlterField(
            model_name='address',
            name='line_one',
            field=models.CharField(blank=True, max_length=128),
        ),
        migrations.AlterField(
            model_name='entity',
            name='display_name',
            field=models.CharField(default=None, max_length=128),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='entity',
            name='email_address',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
    ]
