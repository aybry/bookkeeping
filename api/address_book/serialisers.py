import logging
from pprint import pformat

from rest_framework import serializers, fields

from address_book.models import Entity, Address, BankAccount

logger = logging.getLogger(__name__)


class AddressSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Address
        fields = "__all__"


class BankAccountSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = BankAccount
        fields = "__all__"


class EntitySerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    address = AddressSerializer()

    class Meta:
        model = Entity
        fields = "__all__"

    def create(self, validated_data):
        logger.debug(pformat(validated_data))
        address_data = validated_data.pop("address")
        address = Address.objects.create(
            user=self.context["request"].user, **address_data
        )
        entity = Entity.objects.create(
            user=self.context["request"].user, address=address, **validated_data
        )
        return entity

    def update(self, instance: Entity, validated_data):
        logger.info(pformat(validated_data))
        address_data = validated_data.pop("address")
        Address.objects.filter(pk=instance.address.pk).update(**address_data)
        Entity.objects.filter(pk=instance.pk).update(**validated_data)
        instance = Entity.objects.get(pk=instance.pk)
        return instance
