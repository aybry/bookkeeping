import re
from django.core.exceptions import ValidationError


def phone_number_validator(value):
    if not re.match(
        "^[(+)]{0,1}[0]{0,2}[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$", value
    ):
        raise ValidationError("Phone number not valid")
