# Generated by Django 3.2.14 on 2022-07-23 21:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeping', '0013_document_types'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='line',
            name='footer',
        ),
        migrations.RemoveField(
            model_name='line',
            name='user',
        ),
        migrations.RemoveField(
            model_name='document',
            name='footer',
        ),
        migrations.AddField(
            model_name='document',
            name='document_number_label',
            field=models.CharField(max_length=64, null=True),
        ),
        migrations.AddField(
            model_name='documenttemplate',
            name='document_number_label',
            field=models.CharField(max_length=64, null=True),
        ),
        migrations.AddField(
            model_name='documenttemplate',
            name='html_template',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='bookkeeping.htmltemplate'),
        ),
        migrations.DeleteModel(
            name='Footer',
        ),
        migrations.DeleteModel(
            name='Line',
        ),
    ]
