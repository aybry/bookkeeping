# Generated by Django 3.2.14 on 2022-07-15 22:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeping', '0006_language_to_locale'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='bottom_text',
            field=models.TextField(blank=True, max_length=4096, null=True),
        ),
        migrations.AddField(
            model_name='invoice',
            name='top_text',
            field=models.TextField(blank=True, max_length=4096, null=True),
        ),
    ]
