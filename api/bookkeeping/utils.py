from datetime import datetime
from babel.dates import format_date


def entry_date_to_str(dt: datetime, locale: str) -> str:
    return format_date(dt, format="dd MMM yyyy", locale=locale)
