import logging

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from bookkeeping.models import Entry, Document, DocumentTypeChoices
from address_book.models import Entity


logger = logging.getLogger(__name__)
User = get_user_model()


class Command(BaseCommand):
    help = "Summarises open entries"

    def handle(self, *args, **options):
        unpaid_invoices = Document.objects.filter(
            is_paid=False, document_type=DocumentTypeChoices.INVOICE
        )
        guilty_entities = {inv.recipient for inv in unpaid_invoices}

        if guilty_entities:
            print("\n--- Open Invoices ---\n")

        for entity in guilty_entities:
            print(f"{entity.display_name}:")
            for invoice in unpaid_invoices.filter(recipient=entity):
                print(f"\t{invoice.identifier}")
                for entry in invoice.entries.all():
                    print(
                        f"\t\t{entry.date}:\t{entry.subtotal:.2f}\t{entry.description}"
                    )
                print(f"\t\tTotal: {invoice.total_net:.2f} €\n")

        unpaid_uninvoiced_entries = Entry.objects.filter(is_paid=False, document=None)
        uninformed_entities = {entry.client for entry in unpaid_uninvoiced_entries}

        if uninformed_entities:
            print("\n--- Open Items ---\n")
        for entity in uninformed_entities:
            print(f"{entity.display_name}:")
            for entry in unpaid_uninvoiced_entries.filter(client=entity):
                if len(entry.description) > 97:
                    description = entry.description[:100] + "..."
                else:
                    description = entry.description[:100]
                print(f"\t{entry.date}:\t{entry.subtotal:.2f} €\t{description}")
            sum_unpaid = sum(
                entry.subtotal
                for entry in unpaid_uninvoiced_entries.filter(client=entity)
            )
            print(f"\tTotal: {sum_unpaid:.2f} €\n")
