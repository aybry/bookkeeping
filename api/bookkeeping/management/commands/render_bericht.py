import logging

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.db.models import Q, QuerySet, Sum

from bookkeeping.models import Entry
from address_book.models import Entity


logger = logging.getLogger(__name__)
User = get_user_model()


class Command(BaseCommand):
    help = "Writes summary of open entries for reporting to atene KOM."

    def add_arguments(self, parser):
        parser.add_argument("-e", "--entity-slug", default="atene", nargs="?")

    def handle(self, *args, **options):
        entity_slug = options["entity_slug"]
        entity = Entity.objects.get(slug=entity_slug)

        entries_base_set: QuerySet[Entry] = Entry.objects.filter(
            client=entity, is_paid=False
        ).filter(~Q(document__document_type="qt"))

        dates = (
            entries_base_set.values("date")
            .annotate(total_hours=Sum("unit_number"))
            .order_by("date")
        )
        for date_dict in dates:
            print(f"\n{date_dict['date']} ({date_dict['total_hours']:.1f} Stunden)")
            for entry in entries_base_set.filter(date=date_dict["date"]).order_by(
                "created_at"
            ):
                if entry.project:
                    print(
                        f"- ({entry.unit_number:.1f} {entry.project.name}) {entry.description}"
                    )
                else:
                    print(f"- ({entry.unit_number:.1f}) {entry.description}")
        print("\n")

        projects_hours = (
            entries_base_set.values("project__name")
            .annotate(hours_total=Sum("unit_number"))
            .order_by("project")
        )
        for project_dict in projects_hours:
            print(
                f"Hours Project {project_dict['project__name']}: {project_dict['hours_total']}"
            )
        print(f"Total hours: {sum((entry.unit_number for entry in entries_base_set))}")
