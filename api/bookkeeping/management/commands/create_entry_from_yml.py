import datetime
import logging
import yaml
from pathlib import Path

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from bookkeeping.models import Entry
from address_book.models import Entity


logger = logging.getLogger(__name__)
User = get_user_model()


class Command(BaseCommand):
    """Assumes 60€/hr and atene as client; the latter can be specified before quantity.

    Format of yml entries:

    2023-01-18:
        - "EKKE (2) Adjust script to compute totals itself if totals contain '0,00'"
        - "(5) vom Kunden ergänzte Bugliste mit L.G. gründlich durchgehen, Fragen des Kunde
    """

    help = "Creates Entry by parsing ./data/import.yml"

    def handle(self, *args, **options):
        import_yml = (
            Path("bookkeeping") / "management" / "commands" / "data" / "import.yml"
        )

        with import_yml.open() as f:
            import_data = yaml.load(f.read(), Loader=yaml.FullLoader)

        for date, tasks in import_data.items():
            for task_line in tasks:
                task = TaskFromYaml(date, task_line)
                task.to_entry()


class TaskFromYaml(object):
    def __init__(self, date, line):
        # self.date = self._parse_date(date)
        self.date = date
        self.entity, self.quantity, self.description = self._parse_line(line)

    def _parse_date(self, date_str):
        return datetime.datetime.strptime(date_str, "%Y-%M-%d").date()

    def _parse_line(self, line):
        if line.startswith("("):
            entity_str = "atene"
        else:
            entity_str = line.split()[0].lower()
            line = " ".join(line.split()[1:])

        entity = Entity.objects.get(slug=entity_str)
        quantity = float(line.split()[0].replace("(", "").replace(")", ""))
        description = " ".join(line.split()[1:])

        return entity, quantity, description

    def to_entry(self):
        user_aybry = User.objects.get(pk=1)
        entry = Entry.objects.create(
            user=user_aybry,
            description=self.description,
            unit_price=60,
            unit_number=self.quantity,
            client=self.entity,
        )
        entry.date = self.date
        entry.save()
        entry = Entry.objects.get(pk=entry.pk)

        print(
            f"Created entry: {entry}\n\tClient: {entry.client}\n\tDescription: {entry.description}\n\tHours: {entry.unit_number} @ {entry.unit_price} € ({float(entry.subtotal)} €)\n\t"
        )
