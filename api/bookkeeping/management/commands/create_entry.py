import logging

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from bookkeeping.models import Entry
from address_book.models import Entity


logger = logging.getLogger(__name__)
User = get_user_model()


class Command(BaseCommand):
    help = "Creates Entry via command line"

    def add_arguments(self, parser):
        parser.add_argument("-n", "--num-hours", nargs="?")
        parser.add_argument("-e", "--entity-slug", nargs="?")
        parser.add_argument("-d", "--description", nargs="?")
        parser.add_argument("-p", "--unit-price", nargs="?", default=60)

    def handle(self, *args, **options):
        num_hours = options["num_hours"]
        entity_slug = options["entity_slug"]
        description = options["description"]
        unit_price = options["unit_price"]

        entity = Entity.objects.get(slug=entity_slug)
        user_aybry = User.objects.get(pk=1)

        entry = Entry.objects.create(
            user=user_aybry,
            description=description,
            unit_price=unit_price,
            unit_number=num_hours,
            client=entity,
        )
        entry = Entry.objects.get(pk=entry.pk)

        print(
            f"Created entry: {entry}\n\tClient: {entry.client}\n\tDescription: {entry.description}\n\tHours: {entry.unit_number} @ {entry.unit_price} € ({float(entry.subtotal)} €)\n\t"
        )
