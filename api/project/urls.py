from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from project.healthcheck import are_you_healthy
from bookkeeping.views import (
    UserViewSet,
    EntryViewSet,
    HtmlTemplateViewSet,
    DocumentTemplateViewSet,
    InvoiceViewSet,
    QuoteViewSet,
    RenderDocumentViewSet,
)
from address_book.views import (
    EntityViewSet,
    AddressViewSet,
    BankAccountViewSet,
)


router = routers.DefaultRouter()
router.register("entries", EntryViewSet, basename="Entry")
router.register("html_templates", HtmlTemplateViewSet)
router.register("invoice_templates", DocumentTemplateViewSet)
router.register("invoices", InvoiceViewSet)
router.register("quotes", QuoteViewSet)
router.register("render_invoice", RenderDocumentViewSet)
router.register("users", UserViewSet)

router.register("entities", EntityViewSet)
router.register("addresses", AddressViewSet)
router.register("bankaccounts", BankAccountViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("admin/", admin.site.urls),
    path("are_you_healthy", are_you_healthy, name="healthcheck"),
    path("api/token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("api/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
]
