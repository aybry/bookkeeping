import requests
import sys
from django.http import HttpResponse


def are_you_healthy(_):
    return HttpResponse("I am fine, thanks.")


def check_health():
    url = "http://localhost:8008/are_you_healthy"
    try:
        requests.get(url).raise_for_status()
    except Exception:
        sys.exit(1)


if __name__ == "__main__":
    check_health()
